# Some soft to control and work with Geoscan's propeller.

--------------------

![Motor](https://pp.userapi.com/c845016/v845016174/5d6de/Sa758tN3OfI.jpg)

![KEk](https://pp.userapi.com/c630518/v630518846/3e8bf/GZIJ55QVLdM.jpg)

--------------------

So, what we got there? Uhhh... 

1. TensoWeights_control - arduino leonardo sketch to extract data from tensoweights

2. driver_rs485 - driver for SKLAB-RS485

3. motor_control - soft to control Geoscan's motor

4. Parser - some stuff to parse data from TensoWeights_control. Optional using.